import { Router } from "express";
import { getArticles, getArticlesAuthor, getArticlesTags, postArticlesTitle, deleteArticle } from "../controllers/articles.controller.js";
const router = Router()

router.get('/articles', getArticles)
router.get('/:author', getArticlesAuthor)
router.get('/tags/:tags', getArticlesTags)
router.post('/title', postArticlesTitle)
router.delete('/:id', deleteArticle)

export default router;