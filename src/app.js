import express from "express";
import morgan from "morgan";




//Importing Routes
import articlesRoutes from "./routes/articles.js";

const app = express()

//Middlewares
app.use(morgan('dev'))
app.use(express.json())
//Routes
app.use('/api/articles', articlesRoutes)

export default app 