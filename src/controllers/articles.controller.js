import fetch from "node-fetch"
import sequelize from "sequelize"
import Articles from "../models/articles.js"

const Op = sequelize.Op

export const articleStorage = () => {

    setInterval(async () => {
        try {
            const response = await fetch('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            const data = await response.json()
            const articles = data['hits']
            for (const article of articles) {
                console.log(article._tags)
                const tags = article._tags
                await Articles.create({
                    title: article.title,
                    author: article.author,
                    tags: tags.toString(),
                    comment: article.comment_text,
                    story_title: article.story_title,
                    story_url: article.story_url
                })
            }
            console.log('The insert of the article in the table articles has been a succesfull')

        } catch (error) {
            console.log(error)
        }
    }, 3600000)
}

export const getArticles = async (req, res) => {

    try {
        const { page, size } = req.query
        const articles = await Articles.findAndCountAll({
            limit: size,
            offset: page * size
        })
        
        res.json({
            data: articles
        })

    } catch (error) {
        console.log(error)
    }
}

export const getArticlesAuthor = async (req, res) => {
    try {
        const { author } = req.params
        const article = await Articles.findOne({
            where: {
                author
            }
        })
        res.json(article)
    } catch (error) {
        console.log(error)
    }
}
export const getArticlesTags = async (req, res) => {
    try {
        const { tags } = req.params
        const article = await Articles.findAll({
            where: {
                tags: { [Op.substring]: tags }

            }
        })
        res.json(article)
    } catch (error) {
        console.log(error)
    }
}

export const postArticlesTitle = async (req, res) => {
    try {
        const { title } = req.body
        const article = await Articles.findAll({
            where: {
                title
            }
        })
        res.json(article)
    } catch (error) {
        console.log(error)
    }
}

export const deleteArticle = async (req, res) => {
    try {
        const { id } = req.params
        const deleteRowCount = await Articles.destroy({
            where: {
                id
            }
        })
        res.json({
            message: 'Article deleted succesfully',
            count: deleteRowCount
        })
    } catch (error) {
        console.log(error)
    }

}




