import Sequelize from "sequelize"
import {sequelize} from "../database/database.js"

const Articles = sequelize.define('articles', {
    title:{
        type: Sequelize.TEXT,
    },
    author: {
        type: Sequelize.TEXT
    },
    tags: {
        type: Sequelize.TEXT
    },
    comment:{
        type: Sequelize.TEXT
    },
    story_title:{
        type: Sequelize.TEXT
    }, 
    story_url: {
        type: Sequelize.TEXT
    }
}, {
    timestamps: false
});

export default Articles