
import request  from "supertest";
import app from '../src/app.js'

//Search all the articles
describe('GET /api/articles/articles', () => {
    test('should respond with a 200 status code', async () => {
      const response =  await request(app).get('/api/articles/articles?page=1&size=5').send()
      expect(response.statusCode).toBe(200)
    })

    test('Should respond with an object', async () => {
        const response =  await request(app).get('/api/articles/articles?page=1&size=5').send()
        expect(response.body).toBeInstanceOf(Object)
    })
})
//Search by Author
describe('GET /api/articles/:author', () => {
    test('should respond with a 200 status code', async () => {
      const response =  await request(app).get('/api/articles/forty').send()
      expect(response.statusCode).toBe(200)
    })

    test('Should respond with an object', async () => {
        const response =  await request(app).get('/api/articles/forty').send()
        expect(response.body).toBeInstanceOf(Object)
    })
})
//Search by a tags
describe('GET /api/articles/tags/:tags', () => {
    test('should respond with a 200 status code', async () => {
      const response =  await request(app).get('/api/articles/tags/author_ktretiak').send()
      expect(response.statusCode).toBe(200)
    })

    test('Should respond with an object', async () => {
        const response =  await request(app).get('/api/articles/tags/author_ktretiak').send()
        expect(response.body).toBeInstanceOf(Object)
    })
})
//Search by a title
describe('POST /api/articles/title', () => {
  test('should respond with a 200 status code', async () => {
    const response =  await request(app).post('/api/articles/title').send({
      title: "Launch HN: Nyckel (YC W22) – Train and deploy ML classifiers in minutes"
    })
    expect(response.statusCode).toBe(200)
  })

  test('Should respond with an object', async () => {
      const response =  await request(app).post('/api/articles/title').send({
        title: "Launch HN: Nyckel (YC W22) – Train and deploy ML classifiers in minutes"
      })
      expect(response.body).toBeInstanceOf(Object)
  })
  test('Should have a content-type application/json in header', async ()=>{
      const response =  await request(app).post('/api/articles/title').send({
        title: "Launch HN: Nyckel (YC W22) – Train and deploy ML classifiers in minutes"
      })
      expect(response.headers['content-type']).toEqual(
          expect.stringContaining("json")
      )
  })

})
//Delete an article
describe('Delete /api/articles/:id', () => {
  test('should respond with a 200 status code', async () => {
    const response =  await request(app).delete('/api/articles/13').send()
    expect(response.statusCode).toBe(200)
  })

  test('Should respond with an object', async () => {
      const response =  await request(app).delete('/api/articles/13').send()
      expect(response.body).toBeInstanceOf(Object)
  })
})



