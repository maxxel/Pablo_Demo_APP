# App Articles

This demo app is for the company Reign all rights reserved.
The main idea of this project is consumed the services of the api Hacker News and then create different endpoints which will do diferent searches and delete certain articles as the user wish.

## Installation

App Articles requires [Node.js](https://nodejs.org/) v14.17.6+ to run.
App Articles requires [Postgres](https://www.postgresql.org/) v10.19.
App Articles requires [Postman](https://www.postman.com/).

Install the dependencies and devDependencies and start the server.

```sh
cd js_proyect
npm i
node app
```
#### Creating the DB
Aditionally you need to run the sql sentence to create the bd and the table, you can find this sentences in the folder called sql.


### Put new data into the DB 

For put new data into the db and have data for work, first of all you have to go to the following route:

```
js_project
|
└───src
│   │   controllers
│   │    │
│   │    └─- articles-controlers.js
```
Once you are there you have to modify the setinterval function in articleStorage
```js
export const articleStorage = () => {

    setInterval(async () => {
    //code
        }
    }, 60000)
```
This wil set the interval to one minute and you have to wait to see the data into the DB
## Test

You can test this app executing the following command

```sh
npm test
```


### Created by

Pablo Hernández




