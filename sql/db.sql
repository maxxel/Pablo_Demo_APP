create database if not exists project 

create table if not exists articles(
    id  serial primary key ,
    title text ,
    author text not null check (author <> ''),
    tags text not null check (tags <> ''),
    comment text ,
    story_title text,
    story_url text
);